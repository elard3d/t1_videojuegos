using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    private SpriteRenderer _renderer;
    private Animator _animator;
    private Rigidbody2D _rigidbody;

    private int murio = 0;
    void Start()
    {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        
            _renderer.flipX= false;
            setRunAnimation();
            _rigidbody.velocity = new Vector2(20,_rigidbody.velocity.y);
        
        

        if (Input.GetKeyDown(KeyCode.Space) )
        {
            var upSpeed = 35f;
            _rigidbody.velocity = Vector2.up * upSpeed;
            setJumpAnimation();
            
        }

        if (murio == 1)
        {
            setDeadAnimation();
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.layer == 9)
        {
            Debug.Log("Murio");
            murio = 1;

        }
            
    }
    
    

    private void setIdleAnimation(){
        _animator.SetInteger("Estado",0);
    }
    
    private void setRunAnimation(){
        _animator.SetInteger("Estado",1);
    }
    
    private void setJumpAnimation(){
        _animator.SetInteger("Estado",2);
    }
    
    private void setDeadAnimation(){
        _animator.SetInteger("Estado",3);
    }
    
    
}
    


