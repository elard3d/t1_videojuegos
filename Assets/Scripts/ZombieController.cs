using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    // Start is called before the first frame update

    private SpriteRenderer _renderer;
    private Animator _animator;
    private Rigidbody2D _rigidbody;
    
    void Start()
    {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
       
        
            _rigidbody.velocity = new Vector2(-20, _rigidbody.velocity.y);

        
    }
}
